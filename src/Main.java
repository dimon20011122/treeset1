import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        TreeSet<User> myUserTreeSet = new TreeSet<User>();
        User user1 = new User(798, "Ivan", 62);
        User user2 = new User(876, "Petr", 35);
        User user3 = new User(364, "Vasilii", 46);

        myUserTreeSet.add(user1);
        myUserTreeSet.add(user2);
        myUserTreeSet.add(user3);

        for (User h : myUserTreeSet) {
            System.out.println(h);

        }
    }
}
