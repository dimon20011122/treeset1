import java.util.Comparator;

public class User implements Comparable<User> {

        private int id;
        private String name;
        private int age;

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }



        public User(int id, String name, int age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }

        @Override
        public int compareTo(User emp) {

            return (this.id - emp.id);
        }

        @Override

        public String toString() {
            return "[id=" + this.id + ", name=" + this.name + ", age=" + this.age  + "]";
        }

    }

